import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.BankAccountPage;
import pages.LoginPage;
import pages.HomePage;
/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class e2e {
    public void registerBankAccount(WebDriver driver) throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        HomePage homePage = new HomePage(driver);
        BankAccountPage bankAccountPage = new BankAccountPage(driver);
        driver.get("https://app.es.sageone.com/login");
        //1.	Log in to the application (https://app.es.sageone.com/login) with the following credentials: [User: automatic.test@yopmail.com / Pass: automatic.test]
        loginPage.login();
        //2.	Navigate to the list of bank accounts in the top menu.
        homePage.goToBankAccounts(driver);
        //3.	Try to register a bank account with the following data (“Configurar cuentas bancarias” > “Añadir nueva cuenta”):
        bankAccountPage.createAccountFail(driver);
        //4.	Check that data validation fails when saving. There is one validation on client and one on server.
        Assert.assertTrue("No error message has appear.", driver.findElement(By.xpath("//*[contains(@class, 'icon-error')]")).isDisplayed());
        //5.	Add the name "Bank Account Test" and change the account number to 1111 1111 30 1111111111
        bankAccountPage.endCreateAccount(driver);
        //6.	Save, and check that the new account with all data entered is shown in the Bank Accounts grid.
        bankAccountPage.checkIfRegistered(driver);
    }
}
