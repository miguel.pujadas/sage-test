import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class testChrome extends e2e{
    private static WebDriver driver;
    @BeforeMethod(alwaysRun = true)
    public void setup() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        driver = new ChromeDriver(capability);
        driver.manage().window().maximize();
    }
    @Test
    public void circuit() throws Exception {
        registerBankAccount(driver);
    }
    @AfterMethod(alwaysRun = true)
    public void teardown() {
        //driver.quit();
    }
}
