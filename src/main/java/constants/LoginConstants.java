package constants;

/**
 * Created by Miguel Pujadas Palenzuela
 */
public class LoginConstants {
    public static final String USER_ID = "sso_Email";
    public static final String PASSWORD_ID = "sso_Password";
    public static final String LOGIN_BUTTON_XPATH= "//*[contains(@class, 'button primary')]";
}
