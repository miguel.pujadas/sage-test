package constants;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class BankAccountConstants {
    //MAIN
    public static final String CONFIG_BANK_ACCOUNT_BUTTON_XPATH= "//*[contains(@class, 'UIMultiActionButton')]";
    public static final String ADD_BANK_ACCOUNT_BUTTON_XPATH= "//*[@id='ui-main']/div/div[2]/div[1]/div/div/ul/li[2]/a";
    //NEW ACCOUNT
    public static final String BANK_ACCOUNT_NAME_ID= "bank_account_account_name";
    public static final String BANK_ACCOUNT_TYPE_ID= "bank_account_account_type_id";
    public static final String BANK_ACCOUNT_NUMBER_ID= "bank_account_account_number";
    public static final String BANK_ACCOUNT_SAVE_XPATH= "//*[contains(@class, 'save')]";
    public static final String TOTAL_PAGES_XPATH= "//*[contains(@class, 'total')]";
    public static final String ACTUAL_PAGE_ID= "current_page";
}
