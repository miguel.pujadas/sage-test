package implementations;
import details.BankAccountDetails;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class BankAccountImplementation extends BankAccountDetails{
    public BankAccountImplementation(){
        super();
        setName("Bank Account Test");
        setNumber("1111 1111 40 1111111111");
        setType("Cuenta Corriente");
    }
    public BankAccountImplementation(String name, String number, String type){
        super();
        if(name!=null)setName(name);
        else
            setName("Bank Account Test");
        if(number!=null)setNumber(number);
        else
            setNumber("1111 1111 40 1111111111");
        if(type!=null)setType(type);
        else
            setType("Cuenta Corriente");
    }
}
