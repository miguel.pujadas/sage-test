package implementations;
import details.LoginDetails;
/**
 * Created by Miguel Pujadas Palenzuela
 */
public class LoginImplementation extends LoginDetails{
    public LoginImplementation(){
        super();
        setUser("automatic.test@yopmail.com");
        setPassword("automatic.test");
    }
    public LoginImplementation(String user, String password){
        super();
        if(user!=null)setUser(user);
        else
        setUser("automatic.test@yopmail.com");
        if(password!=null)setPassword(password);
        else
        setPassword("automatic.test");
    }
}
