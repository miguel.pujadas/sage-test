package factories;
import details.HomeDetails;
import implementations.HomeImplementation;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class HomeFactory {
    public static HomeDetails createHomeData(){
        return new HomeImplementation();
    }
}
