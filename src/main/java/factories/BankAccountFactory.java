package factories;
import details.BankAccountDetails;
import implementations.BankAccountImplementation;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class BankAccountFactory {
    public static BankAccountDetails createBankAccountData(){
        return new BankAccountImplementation();
    }
    public static BankAccountDetails createBankAccountData(String name, String number, String type){
        return new BankAccountImplementation(name, number, type);
    }
}
