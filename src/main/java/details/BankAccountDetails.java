package details;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import constants.BankAccountConstants;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class BankAccountDetails {
    public String name;
    public String type;
    public String number;
    public BankAccountDetails(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    public BankAccountDetails(){}

    @FindBy(id = BankAccountConstants.BANK_ACCOUNT_NAME_ID)
    private WebElement nameInput;
    public WebElement getNameInput() {return nameInput;}
    public void setName(String name) {this.name = name;}
    public String getName(){return name;}

    @FindBy(id = BankAccountConstants.BANK_ACCOUNT_TYPE_ID)
    private WebElement typeInput;
    public WebElement getTypeInput() {return typeInput;}
    public void setType(String type) {this.type = type;}
    public String getType(){return type;}

    @FindBy(id = BankAccountConstants.BANK_ACCOUNT_NUMBER_ID)
    private WebElement numberInput;
    public WebElement getNumberInput() {return numberInput;}
    public void setNumber(String number) {this.number = number;}
    public String getNumber(){return number;}

    @FindBy(xpath = BankAccountConstants.ADD_BANK_ACCOUNT_BUTTON_XPATH)
    private WebElement addAccountButton;
    public WebElement getAddAccountButton() {return addAccountButton;}

    @FindBy(xpath = BankAccountConstants.BANK_ACCOUNT_SAVE_XPATH)
    private WebElement saveAccountButton;
    public WebElement getSaveAccountButton() {return saveAccountButton;}

    @FindBy(xpath = BankAccountConstants.CONFIG_BANK_ACCOUNT_BUTTON_XPATH)
    private WebElement configAccountButton;
    public WebElement getConfigAccountButton() {return configAccountButton;}

    @FindBy(xpath = BankAccountConstants.TOTAL_PAGES_XPATH)
    private WebElement totalPages;
    public WebElement getTotalPages() {return totalPages;}

    @FindBy(id = BankAccountConstants.ACTUAL_PAGE_ID)
    private WebElement actualPage;
    public WebElement getActualPage() {return actualPage;}
}
