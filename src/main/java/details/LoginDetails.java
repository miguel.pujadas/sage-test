package details;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import constants.LoginConstants;
/**
 * Created by Miguel Pujadas Palenzuela
 */
public class LoginDetails extends LoginConstants{
    public String user;
    public String password;
    public LoginDetails(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    public LoginDetails(){}

    @FindBy(id = LoginConstants.USER_ID)
    private WebElement userInput;
    public WebElement getUserInput() {return userInput;}
    public void setUser(String user) {this.user = user;}
    public String getUser(){return user;}

    @FindBy(id = LoginConstants.PASSWORD_ID)
    private WebElement passwordInput;
    public WebElement getUserPasswordInput() {return passwordInput;}
    public void setPassword(String password) {this.password = password;}
    public String getPassword(){return password;}

    @FindBy(xpath = LoginConstants.LOGIN_BUTTON_XPATH)
    private WebElement loginButton;
    public WebElement getLoginButton() {return loginButton;}
}
