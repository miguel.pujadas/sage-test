package details;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import constants.HomeConstants;
/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class HomeDetails {
    public HomeDetails(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    public HomeDetails(){}
    @FindBy(xpath = HomeConstants.BANK_ACCOUNTS_BUTTON_ID)
    private WebElement bankAccountsButton;
    public WebElement getBankAccountsButton() {return bankAccountsButton;}
}
