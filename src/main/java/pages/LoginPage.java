package pages;
import org.openqa.selenium.WebDriver;
import details.LoginDetails;
import factories.LoginFactory;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class LoginPage {
    private static LoginDetails pageFactory;
    public LoginPage(WebDriver driver){
        this.pageFactory = new LoginDetails(driver);
    }

    public static void login(){
        LoginDetails loginDetails = LoginFactory.createLoginData();
        pageFactory.getUserInput().sendKeys(loginDetails.getUser());
        pageFactory.getUserPasswordInput().sendKeys(loginDetails.getPassword());
        pageFactory.getLoginButton().click();
    }
}
