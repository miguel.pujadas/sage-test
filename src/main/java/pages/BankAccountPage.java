package pages;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import details.BankAccountDetails;
import factories.BankAccountFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class BankAccountPage {
    private static BankAccountDetails pageFactory;
    public BankAccountPage(WebDriver driver){
        this.pageFactory = new BankAccountDetails(driver);
    }

    public static void createAccountFail(WebDriver driver){
        BankAccountDetails banckAccountDetails = BankAccountFactory.createBankAccountData();
        tools.Selenium tools = new tools.Selenium();
        tools.waitToElement(driver, By.xpath("//*[contains(@class, 'UIMultiActionButton')]"),30);
        pageFactory.getConfigAccountButton().click();
        pageFactory.getAddAccountButton().click();
        tools.waitToElement(driver, By.id("bank_account_account_number"),30);
        pageFactory.getNumberInput().sendKeys(banckAccountDetails.getNumber());
        pageFactory.getTypeInput().sendKeys(banckAccountDetails.getType());
        pageFactory.getSaveAccountButton().click();
        tools.waitToElement(driver, By.xpath("//*[contains(@class, 'icon-error')]"),5);
    }

    public static void endCreateAccount(WebDriver driver){
        BankAccountDetails banckAccountDetails = BankAccountFactory.createBankAccountData();
        pageFactory.getNameInput().sendKeys(banckAccountDetails.getName());
        pageFactory.getNumberInput().clear();
        pageFactory.getNumberInput().sendKeys("1111 1111 30 1111111111");
        pageFactory.getSaveAccountButton().click();
    }

    public static void checkIfRegistered(WebDriver driver) throws InterruptedException {
        tools.Selenium tools = new tools.Selenium();
        //We know the last record is always stored in the last register of the grid, so we requiere to know the number of pages the grid has and move to the last one
        tools.waitToElement(driver,By.id("current_page"),5);
        pageFactory.getActualPage().sendKeys(pageFactory.getTotalPages().getText());
        pageFactory.getActualPage().sendKeys(Keys.ENTER);
        //Now we are in the last page, and we still know that our new account will be the last one.
        //It's impossible to automize a click in any element in the table because although the page only shows a maximum of 10 elements, all the stored elements are hide overlapping
        //the newerts. So the only way I have to check that the account has been created is looking in the DOM for the highest value.
        //I hate explicit waits, but I had to do because the transition between page 1 to the last page can not be registered.
        Thread.sleep(2000);
        //In case there is only one register, there will not be and odd element, so we must cover it with an exception. In this case, we are sure that the biggest number will be even.
        try {
            if (Integer.parseInt(driver.findElement(By.xpath("//*[contains(@class, 'odd')][last()]")).getAttribute("data-id")) > Integer.parseInt(driver.findElement(By.xpath("//*[contains(@class, 'even')][last()]")).getAttribute("data-id"))) {
                driver.get("https://app.es.sageone.com/gestion/banking/bank_accounts/" + driver.findElement(By.xpath("//*[contains(@class, 'odd')][last()]")).getAttribute("data-id"));
            } else {
                driver.get("https://app.es.sageone.com/gestion/banking/bank_accounts/" + driver.findElement(By.xpath("//*[contains(@class, 'even')][last()]")).getAttribute("data-id"));
            }
        }
        catch (Exception e){
            driver.get("https://app.es.sageone.com/gestion/banking/bank_accounts/" + driver.findElement(By.xpath("//*[contains(@class, 'even')][last()]")).getAttribute("data-id"));
        }
        tools.waitToElement(driver,By.id("current_page"),5);
        Assert.assertEquals(driver.findElement(By.id("bank_account_account_name")).getAttribute("value"),"Bank Account Test");
        Assert.assertEquals(driver.findElement(By.id("bank_account_account_type_id")).getAttribute("value"),"Cuenta Corriente");
        Assert.assertEquals(driver.findElement(By.id("bank_account_account_number")).getAttribute("value"),"1111-1111-30-1111111111");
    }
}
