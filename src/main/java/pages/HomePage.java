package pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import details.HomeDetails;

/**
 * Created by Miguel Pujadas Palenzuela.
 */
public class HomePage {
    private static HomeDetails pageFactory;
    public HomePage (WebDriver driver){
        this.pageFactory = new HomeDetails(driver);
    }

    public static void goToBankAccounts(WebDriver driver){
        tools.Selenium tools = new tools.Selenium();
        tools.waitToElement(driver, By.xpath("//*[contains(@href, '/bank_accounts')]"),30);
        pageFactory.getBankAccountsButton().click();
    }
}
